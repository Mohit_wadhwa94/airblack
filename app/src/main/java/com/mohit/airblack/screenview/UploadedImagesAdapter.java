package com.mohit.airblack.screenview;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mohit.airblack.R;
import com.mohit.airblack.common.Utility;
import com.mohit.airblack.data.album.ImageUploadData;
import com.squareup.picasso.Picasso;

import java.util.List;

public class UploadedImagesAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private List<ImageUploadData> imageUploadData;

    public UploadedImagesAdapter(Activity activity, List<ImageUploadData> list) {
        this.activity = activity;
        this.imageUploadData = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.gallery_image_grid_entity, viewGroup, false);
        return new ImageViewHolder(view, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ImageViewHolder) {
            ((ImageViewHolder) viewHolder).setImage(imageUploadData.get(i).getImageURL());
        }
    }

    @Override
    public int getItemCount() {
        return imageUploadData.size();
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder {
        private static final String TAG = ImageViewHolder.class.getName();
        private ImageView imageView;
        private Activity activity;

        public ImageViewHolder(View itemView, Activity activity) {
            super(itemView);
            this.activity = activity;
            imageView = itemView.findViewById(R.id.gallery_image_view);
        }

        public void setImage(String path) {
            int dp_100 = (int) Utility.ConvertDpToPixel(100, activity);
            imageView.getLayoutParams().height = dp_100;

            Picasso.with(activity)
                    .load(path)
                    .resize(dp_100, dp_100)
                    .centerCrop()
                    .placeholder(R.drawable.ic_photo_placeholder)
                    .into(imageView);

            itemView.setOnClickListener(v -> {
                Intent intent = new Intent(activity, ImageFullScreenActivity.class);
                intent.putExtra("image", path);
                activity.startActivity(intent);
            });
        }

    }
}
