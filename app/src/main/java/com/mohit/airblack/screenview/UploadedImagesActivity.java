package com.mohit.airblack.screenview;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mohit.airblack.R;
import com.mohit.airblack.data.album.ImageUploadData;
import com.mohit.airblack.screenupload.ImageUploadActivity;

import java.util.ArrayList;
import java.util.List;

public class UploadedImagesActivity extends AppCompatActivity {

    private List<ImageUploadData> list;
    private UploadedImagesAdapter adapter;
    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;
    private TextView noData;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.uploaded_images_activity);

        init();
    }

    private void init() {
        noData = findViewById(R.id.no_data);
        recyclerView = findViewById(R.id.recycler_view);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Images...");
        progressDialog.show();

        list = new ArrayList<>();

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference(ImageUploadActivity.databasePath);
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot postSnapshot : snapshot.getChildren()) {

                    ImageUploadData imageUploadInfo = postSnapshot.getValue(ImageUploadData.class);

                    list.add(imageUploadInfo);
                }

                progressDialog.dismiss();
                setData();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                progressDialog.dismiss();
                setData();
            }
        });
    }

    private void setData() {
        if (list == null || list.size() < 1) {
            noData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            recyclerView.setVisibility(View.VISIBLE);
            GridLayoutManager layoutManager = new GridLayoutManager(this, 4,
                    RecyclerView.VERTICAL, false);

            recyclerView.setLayoutManager(layoutManager);
            noData.setVisibility(View.GONE);
            adapter = new UploadedImagesAdapter(UploadedImagesActivity.this, list);
            recyclerView.setAdapter(adapter);
        }
    }
}
