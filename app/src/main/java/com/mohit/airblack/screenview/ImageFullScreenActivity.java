package com.mohit.airblack.screenview;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.github.chrisbanes.photoview.PhotoView;
import com.mohit.airblack.R;
import com.mohit.airblack.common.Utility;
import com.squareup.picasso.Picasso;


public class ImageFullScreenActivity extends AppCompatActivity {

    private String TAG = ImageFullScreenActivity.class.getName();

    private ImageView backButton;
    private PhotoView photoView;

    private String imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_full_screen_activity);

        init();
        handleIntent();

        if (!Utility.isStringValid(imageUrl)) {
            finish();
            Utility.showToast(this, "No Data Found");
            return;
        }

        setData();
    }

    private void init() {
        backButton = findViewById(R.id.back_bt);
        photoView = findViewById(R.id.photo);
    }

    private void handleIntent() {
        Intent intent = getIntent();
        if (intent == null) {
            return;
        }

        imageUrl = intent.getStringExtra("image");
    }

    private void setData() {
        Picasso.with(this)
                .load(imageUrl)
                .into(photoView);

        backButton.setOnClickListener(view -> onBackPressed());
    }
}
