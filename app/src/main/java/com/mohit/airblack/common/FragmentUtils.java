package com.mohit.airblack.common;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class FragmentUtils {
    public static boolean attachFragment(AppCompatActivity activity, String tag) {
        Fragment fragment = activity.getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment != null) {
            activity.getSupportFragmentManager().beginTransaction().attach(fragment).commit();
            return true;
        }

        return false;
    }

    public static void addFragment(AppCompatActivity activity, Fragment fragment, int id, String tag) {
        FragmentManager fragManager = activity.getSupportFragmentManager();
        FragmentTransaction fragTransaction = fragManager.beginTransaction();
        fragTransaction.add(id, fragment, tag);
        fragTransaction.commit();
    }

    public static void popBackStackAllowingStateLosses(FragmentManager fragmentManager) {
        if (fragmentManager == null) {
            return;
        }

        try {
            fragmentManager.popBackStack();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void popBackStackImmediateAllowingStateLosses(FragmentManager fragmentManager) {
        if (fragmentManager == null) {
            return;
        }

        try {
            fragmentManager.popBackStackImmediate();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
