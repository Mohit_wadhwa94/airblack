package com.mohit.airblack.common;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.mohit.airblack.images.ImageUtil;
import com.mohit.airblack.init.AppController;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;

import rx.Observable;

public class BitmapUtils extends AsyncTask<Integer, Void, Bitmap> {
    private final WeakReference<ImageView> imageViewReference;
    private int data = 0;
    private File imageFile = null;
    private int requiredWidth;
    private int requiredHeight;
    private BitmapLoadCallback loaderCallback;
    private Context context;

    String TAG = BitmapUtils.class.getName();

    public interface BitmapLoadCallback {
        void onBitmapLoaded(Bitmap bitmap, File file);
    }

    public BitmapUtils(ImageView imageView, File imageFile,
                       int requiredWidth, int requiredHeight, Context context) {
        imageViewReference = new WeakReference<ImageView>(imageView);
        this.imageFile = imageFile;
        this.requiredWidth = requiredWidth;
        this.requiredHeight = requiredHeight;
        this.context = context;
    }

    public BitmapUtils(File imageFile, int requiredWidth, int requiredHeight,
                       BitmapLoadCallback callback, Context context) {
        // Use a WeakReference to ensure the ImageView can be garbage collected
        imageViewReference = new WeakReference<ImageView>(null);
        this.imageFile = imageFile;
        this.requiredWidth = requiredWidth;
        this.requiredHeight = requiredHeight;
        this.context = context;
        this.loaderCallback = callback;
    }

    // Decode image in background.
    @Override
    protected Bitmap doInBackground(Integer... params) {
        return decodeSampledBitmapFromResource(context.getResources(), imageFile, requiredWidth, requiredHeight);
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if ( bitmap != null ) {
            ImageView imageView = imageViewReference.get();
            if ( imageView != null ) {
                imageView.setImageBitmap(bitmap);
            }

            if ( loaderCallback != null ) {
                loaderCallback.onBitmapLoaded(bitmap, imageFile);
            }
        }
    }

    public int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            try {
                while ((inSampleSize != 0) && ((halfHeight / inSampleSize) > reqHeight) && ((halfWidth / inSampleSize) > reqWidth)) {
                    inSampleSize *= 2;
                }
            } catch (Exception e) {
                Log.e(TAG, e.toString());
                return 1;
            }
        }

        return inSampleSize;
    }

    public Bitmap decodeSampledBitmapFromResource(Resources res, File f,
                                                   int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory.decodeStream(new FileInputStream(f), null, options);


            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            return ImageUtil.loadAndRotateBitmapWithOptions(f.getAbsolutePath(), reqWidth, reqHeight, options);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static class FeedTransformation implements Transformation {

        private String key;
        private WeakReference<ImageView> imageView;

        public FeedTransformation(String url, String locationId,  ImageView imageView) {
            key = url + locationId;
            this.imageView = new WeakReference<>(imageView);
        }

        @Override
        public Bitmap transform(Bitmap source) {
            View image = imageView.get();
            if ( image == null ) {
                return source;
            }

            int width = image.getWidth();
            if ( width <= 0 ) {
                return source;
            }

            int height = (source.getHeight() * width) / source.getWidth();

            Bitmap newBitmap = Bitmap.createScaledBitmap(source, width, height, true);
            if ( newBitmap != source) {
                source.recycle();
            }

            return newBitmap;
        }

        @Override
        public String key() {
            return key;
        }
    }

    public static class BlurredTransformation implements Transformation {

        private final String key;

        public BlurredTransformation(String url, String location) {
            key = url + location;
        }

        @Override
        public Bitmap transform(Bitmap source) {
            Bitmap newBitmap = ImageUtil.SafeFastBlur(source, 50);
            if ( newBitmap != source ) {
                source.recycle();
            }

            Bitmap darkerBmp = ImageUtil.changeBitmapContrastBrightness(newBitmap, 0.7f, -5);
            if ( darkerBmp != newBitmap ) {
                newBitmap.recycle();
            }

            return darkerBmp;
        }

        @Override
        public String key() {
            return key;
        }
    }

    public static Observable<Bitmap> getBitmapFromUrl(String url)  {
        return Observable.create(subscriber -> {
            try {
                Bitmap bitmap = Picasso.with(AppController.getAppContext()).load(url).get();
                subscriber.onNext(bitmap);
                subscriber.onCompleted();
            } catch ( IOException e ) {
                e.printStackTrace();
                subscriber.onError(e);
            }
        });
    }

    public static String writeBitmapToFile(Bitmap bmp) {

        if (bmp == null) {
            return "";
        }

        FileOutputStream out = null;
        String filePath = null;
        try {

            File file = ImageUtil.getOutputMediaFile();
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 90, out);
            filePath = file.getPath();

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return filePath;

    }
}