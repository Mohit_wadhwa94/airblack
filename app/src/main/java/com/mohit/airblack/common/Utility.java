package com.mohit.airblack.common;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Utility {

    private static String TAG = Utility.class.getName();
    private static Context applicationContext;

    public synchronized static void initUtility(Context context) {
        applicationContext = context;
    }

    public static Context GetApplicationContext() {
        return applicationContext;
    }


    public static void showToast(Context context, String message) {

        if (context == null) {
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1
                && context instanceof Activity
                && ((Activity) context).isDestroyed()) {
            return;
        }

        try {
            Toast t = Toast.makeText(context, message, Toast.LENGTH_LONG);
            ((TextView) ((LinearLayout) t.getView()).getChildAt(0))
                    .setGravity(Gravity.CENTER_HORIZONTAL);
            t.show();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
        }
    }

    public static boolean isPermissionGranted(Context context, String permission) {
        try {
            return ActivityCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED;
        } catch (Exception e) {
            return false;
        }
    }

    public static float ConvertDpToPixel(float dp, Context context) {
        if (context == null) {
            Log.e(TAG, "Context is null");
            return 0;
        }

        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * (metrics.densityDpi / 160f);
    }

    public static boolean isStringValid(String stringToValidate) {
        String trimmedStringToValidate = null;

        if (stringToValidate != null) {
            trimmedStringToValidate = stringToValidate.trim();
            return (!trimmedStringToValidate.isEmpty()
                    && !trimmedStringToValidate.equalsIgnoreCase("null")
                    && !trimmedStringToValidate.equalsIgnoreCase("None")
                    && !trimmedStringToValidate.equalsIgnoreCase("na"));
        }

        return false;
    }

    public static boolean IsEmpty(String str) {
        return str == null || str.trim().equals("");
    }

    public static boolean IsNotEmpty(String str) {
        return !IsEmpty(str);
    }

    public static String readFromInputStream(InputStream is) {
        if (is == null) {
            Log.e(TAG, "input stream is null");
            return null;
        }

        InputStreamReader reader = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(reader);

        StringBuilder sb = new StringBuilder();
        String line;

        try {
            while ((line = br.readLine()) != null) {
                sb.append(line);
                sb.append("\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                br.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }

    public static int getActivityWidth(Activity activity) {
        if (activity == null) {
            Log.e(TAG, "Activity sent to function GetScreenWith() is null");
            return -1;
        }

        DisplayMetrics displaymetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics.widthPixels;
    }

    public static void dismissDialog(Dialog progressBar) {
        try {
            if (progressBar != null && progressBar.isShowing()) {
                progressBar.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
