package com.mohit.airblack.images;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;

import com.mohit.airblack.common.BitmapUtils;
import com.mohit.airblack.common.ExifHelper;
import com.mohit.airblack.data.ModelCallback;

import java.io.Closeable;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImageUtil {

    private static String TAG = "ImageUtil";

    private ImageUtil() {
    }

    public static Bitmap rotateBitmap(String fileName, Bitmap resizedBitmap) {
        try {

            // Images are being saved in landscape, so rotate them back to portrait if they were taken in portrait
            Matrix mtx = new Matrix();
            ExifInterface exif = new ExifInterface(fileName);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            Log.e(TAG, "Orientation " + orientation);
            switch (orientation) {
                case ExifInterface.ORIENTATION_UNDEFINED: // Nexus 7 landscape...
                    break;
                case ExifInterface.ORIENTATION_NORMAL: // landscape
                    break;
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    mtx.preRotate(180);
                    resizedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), mtx, false);
                    mtx = null;
                    break;
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    break;
                case ExifInterface.ORIENTATION_TRANSPOSE:
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90: // portrait
                    mtx.preRotate(90);
                    resizedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), mtx, false);
                    mtx = null;
                    break;
                case ExifInterface.ORIENTATION_TRANSVERSE:
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270: // might need to flip horizontally too...
                    mtx.preRotate(270);
                    resizedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), mtx, false);
                    mtx = null;
                    break;
                default:
                    mtx.preRotate(90);
                    resizedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), mtx, false);
                    mtx = null;
                    break;
            }

        } catch (Exception ex) {
            Log.e(TAG, "Error creating bitmap:" + ex);
        }
        return resizedBitmap;
    }

    public static Bitmap loadAndRotateBitmapWithOptions(String fileName, int width, int height, BitmapFactory.Options options) {
        Bitmap resizedBitmap = null;
        try {
            resizedBitmap = BitmapFactory.decodeFile(fileName, options);

            // Images are being saved in landscape, so rotate them back to portrait if they were taken in portrait
            Matrix mtx = new Matrix();
            ExifInterface exif = new ExifInterface(fileName);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
            Log.d(TAG, "Orientation " + orientation);
            switch (orientation) {
                case ExifInterface.ORIENTATION_UNDEFINED: // Nexus 7 landscape...
                    break;
                case ExifInterface.ORIENTATION_NORMAL: // landscape
                    break;
                case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    mtx.preRotate(180);
                    resizedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), mtx, false);
                    mtx = null;
                    break;
                case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                    break;
                case ExifInterface.ORIENTATION_TRANSPOSE:
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90: // portrait
                    mtx.preRotate(90);
                    resizedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), mtx, false);
                    mtx = null;
                    break;
                case ExifInterface.ORIENTATION_TRANSVERSE:
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270: // might need to flip horizontally too...
                    mtx.preRotate(270);
                    resizedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), mtx, false);
                    mtx = null;
                    break;
                default:
                    mtx.preRotate(90);
                    resizedBitmap = Bitmap.createBitmap(resizedBitmap, 0, 0, resizedBitmap.getWidth(), resizedBitmap.getHeight(), mtx, false);
                    mtx = null;
                    break;
            }

        } catch (Exception ex) {
            Log.e(TAG, "Error creating bitmap:" + ex);
        }
        return resizedBitmap;
    }

    // Create an image file to store the selfie
    public static File getOutputMediaFile() {
        return getOutputMediaFile(false);
    }

    public static File getOutputMediaFile(boolean isZip) {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "MagicPin");
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (isZip) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "ZIP_" + timeStamp + ".zip");
        } else {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_" + timeStamp + ".jpg");
        }

        Log.e("FileBackend", "Generating file: " + mediaFile.getAbsolutePath());

        return mediaFile;
    }

    public static File getOutputMediaFileContext(Context context) {
        File externalDir = context.getExternalFilesDir("temp");

        if (externalDir == null) {
            externalDir = context.getFilesDir();
        }

        File mediaStorageDir = new File(externalDir, "MagicPin");
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                "IMG_" + timeStamp + ".jpg");

        Log.e("FileBackend", "Generating file: " + mediaFile.getAbsolutePath());

        return mediaFile;
    }

    public static File getOutputMediaDir() {

        File mediaStorageDir = new File(Environment.getExternalStorageDirectory(), "MagicPin");
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(TAG, "failed to create directory");
                return null;
            }
        }

        return mediaStorageDir;
    }

    public static class SizeDim {
        private int height;
        private int width;

        public SizeDim(int width, int height) {
            this.height = height;
            this.width = width;
        }

        public SizeDim() {
            this.width = 0;
            this.height = 0;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }

        public void setWidth(int width) {
            this.width = width;
        }
    }

    public static SizeDim getScaledSize(SizeDim availableSize, SizeDim imageSize) {

        SizeDim newDim = new SizeDim();
        newDim.setHeight(availableSize.getHeight());
        newDim.setWidth((int) Math.round(
                ((double) imageSize.getWidth()) / ((double) imageSize.getHeight()) * ((double) newDim.getHeight())
        ));

        if (newDim.width > availableSize.getWidth()) {
            Log.e(TAG, "new width > available width" + newDim.getWidth() + "," + availableSize.getWidth());

            newDim.setWidth(availableSize.getWidth());
            newDim.setHeight((int) Math.round(
                    ((double) imageSize.getHeight()) / ((double) imageSize.getWidth()) * ((double) newDim.getWidth())
            ));
        }

        Log.e(TAG, "new size " + newDim.getWidth() + " , " + newDim.getHeight());
        return newDim;
    }

    public static Bitmap cropCenterSquare(Context context, Uri image, int size) {
        if (image == null) {
            return null;
        }
        InputStream is = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = calcSampleSize(context, image, size);
            is = context.getContentResolver().openInputStream(image);
            Bitmap input = BitmapFactory.decodeStream(is, null, options);
            if (input == null) {
                return null;
            } else {
                int rotation = getRotation(context, image);
                if (rotation > 0) {
                    input = rotate(input, rotation);
                }
                return cropCenterSquare(input, size);
            }
        } catch (FileNotFoundException e) {
            return null;
        } finally {
            close(is);
        }
    }

    private static int calcSampleSize(Context context, Uri image, int size) throws FileNotFoundException {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeStream(context.getContentResolver().openInputStream(image), null, options);
        return calcSampleSize(options, size);
    }

    private static int calcSampleSize(BitmapFactory.Options options, int size) {
        int height = options.outHeight;
        int width = options.outWidth;
        int inSampleSize = 1;

        if (height > size || width > size) {
            int halfHeight = height / 2;
            int halfWidth = width / 2;

            while ((halfHeight / inSampleSize) > size
                    && (halfWidth / inSampleSize) > size) {
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    private static int getRotation(Context context, Uri image) {
        InputStream is = null;
        try {
            is = context.getContentResolver().openInputStream(image);
            return ExifHelper.getOrientation(is);
        } catch (FileNotFoundException e) {
            return 0;
        } finally {
            close(is);
        }
    }

    private static void close(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (IOException e) {
            }
        }
    }

    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();
        Matrix mtx = new Matrix();
        mtx.postRotate(degree);
        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    public static Bitmap cropCenterSquare(Bitmap input, int size) {
        int w = input.getWidth();
        int h = input.getHeight();

        float scale = Math.max((float) size / h, (float) size / w);

        float outWidth = scale * w;
        float outHeight = scale * h;
        float left = (size - outWidth) / 2;
        float top = (size - outHeight) / 2;
        RectF target = new RectF(left, top, left + outWidth, top + outHeight);

        Bitmap output = Bitmap.createBitmap(size, size, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);
        canvas.drawBitmap(input, null, target, null);
        return output;
    }

    public static Bitmap createCenterCropBitmap(Bitmap originalBmp, int width, int height) {
        if (originalBmp == null) {
            return null;
        }

        Bitmap scaledBmp;
        float oriWidth = originalBmp.getWidth();
        float oriHeight = originalBmp.getHeight();

        // Match the with and then see.
        float scaleW = (float) width / oriWidth;
        float newWidth = (float) width;
        float newHeight = oriHeight * scaleW;
        if (newHeight < (float) height) {
            float scaleH = (float) height / newHeight;
            newHeight = (float) height;
            newWidth = scaleH * newWidth;
        }

        if (newWidth == 0 || newHeight == 0) {
            return null;
        }

        scaledBmp = Bitmap.createScaledBitmap(originalBmp, (int) newWidth, (int) newHeight, true);

        Rect old = new Rect(0, 0, (int) newWidth, (int) newHeight);
        Rect applied = new Rect();
        Gravity.apply(Gravity.CENTER, scaledBmp.getWidth(), scaledBmp.getHeight(), old, applied);

        Bitmap cropBmp = Bitmap.createBitmap(scaledBmp, applied.left, applied.top, width, height);
        if (cropBmp != scaledBmp) {
            scaledBmp.recycle();
        }

        return cropBmp;
    }

    public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness) {
        if (bmp == null) {
            return null;
        }

        ColorMatrix cm = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
                });

        Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(cm));
        canvas.drawBitmap(bmp, 0, 0, paint);

        return ret;
    }

    public static int dpToPx(Context context, int dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        int px = (int) (dp * scale);
        return px;
    }

    public static void getBitmapFromURL(String src, final ModelCallback<Bitmap> callback) {

        new AsyncTask<String, String, Bitmap>() {
            @Override
            protected Bitmap doInBackground(String... params) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    Bitmap myBitmap = BitmapFactory.decodeStream(input);
                    return myBitmap;
                } catch (IOException e) {
                    Log.e(TAG, "IOException in getBitmapFromURL()", e);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(Bitmap bitmap) {
                if (bitmap != null) {
                    callback.success(bitmap);
                } else {
                    callback.error(null);
                }
            }
        }.execute(src);
    }

    public static void writeFileURLtoSDCard(String src, final String dest, final ModelCallback<String> callback) {

        new AsyncTask<String, String, String>() {
            @Override
            protected String doInBackground(String... params) {
                try {
                    URL url = new URL(params[0]);
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    connection.connect();
                    InputStream input = connection.getInputStream();
                    FileOutputStream fileOutputStream = new FileOutputStream(params[1]);

                    byte buffer[] = new byte[1024];
                    int dataSize;
                    //int loadedSize = 0;
                    while ((dataSize = input.read(buffer)) != -1) {
                        //loadedSize += dataSize;
                        //publishProgress(loadedSize);
                        fileOutputStream.write(buffer, 0, dataSize);
                    }

                    fileOutputStream.close();
                    return "success";

                } catch (IOException e) {
                    Log.e(TAG, "IOException in getBitmapFromURL()", e);
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String result) {
                if (result != null) {
                    callback.success(result);
                } else {
                    callback.error(null);
                }
            }
        }.execute(src, dest);
    }

    public static void LoadImageToView(ImageView imageView, String pathName, int width, int height) {
        BitmapUtils task = new BitmapUtils(imageView, new File(pathName),
                width, height, imageView.getContext());
        task.execute();
    }

    public static Bitmap SafeFastBlur(Bitmap sentBitmap, int radius) {
        try {
            return FastBlur(sentBitmap, radius);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sentBitmap;
    }

    private static Bitmap FastBlur(Bitmap sentBitmap, int radius) {
        if (sentBitmap == null) {
            return null;
        }
        Bitmap bitmap = sentBitmap.copy(sentBitmap.getConfig(), true);

        if (radius < 1) {
            return (null);
        }

        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        int[] pix = new int[w * h];
        Log.e(TAG, w + " " + h + " " + pix.length);
        bitmap.getPixels(pix, 0, w, 0, 0, w, h);

        int wm = w - 1;
        int hm = h - 1;
        int wh = w * h;
        int div = radius + radius + 1;

        int r[] = new int[wh];
        int g[] = new int[wh];
        int b[] = new int[wh];
        int rsum, gsum, bsum, x, y, i, p, yp, yi, yw;
        int vmin[] = new int[Math.max(w, h)];

        int divsum = (div + 1) >> 1;
        divsum *= divsum;
        int dv[] = new int[256 * divsum];
        for (i = 0; i < 256 * divsum; i++) {
            dv[i] = (i / divsum);
        }

        yw = yi = 0;

        int[][] stack = new int[div][3];
        int stackpointer;
        int stackstart;
        int[] sir;
        int rbs;
        int r1 = radius + 1;
        int routsum, goutsum, boutsum;
        int rinsum, ginsum, binsum;

        for (y = 0; y < h; y++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            for (i = -radius; i <= radius; i++) {
                p = pix[yi + Math.min(wm, Math.max(i, 0))];
                sir = stack[i + radius];
                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);
                rbs = r1 - Math.abs(i);
                rsum += sir[0] * rbs;
                gsum += sir[1] * rbs;
                bsum += sir[2] * rbs;
                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }
            }
            stackpointer = radius;

            for (x = 0; x < w; x++) {

                r[yi] = dv[rsum];
                g[yi] = dv[gsum];
                b[yi] = dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (y == 0) {
                    vmin[x] = Math.min(x + radius + 1, wm);
                }
                p = pix[yw + vmin[x]];

                sir[0] = (p & 0xff0000) >> 16;
                sir[1] = (p & 0x00ff00) >> 8;
                sir[2] = (p & 0x0000ff);

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[(stackpointer) % div];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi++;
            }
            yw += w;
        }
        for (x = 0; x < w; x++) {
            rinsum = ginsum = binsum = routsum = goutsum = boutsum = rsum = gsum = bsum = 0;
            yp = -radius * w;
            for (i = -radius; i <= radius; i++) {
                yi = Math.max(0, yp) + x;

                sir = stack[i + radius];

                sir[0] = r[yi];
                sir[1] = g[yi];
                sir[2] = b[yi];

                rbs = r1 - Math.abs(i);

                rsum += r[yi] * rbs;
                gsum += g[yi] * rbs;
                bsum += b[yi] * rbs;

                if (i > 0) {
                    rinsum += sir[0];
                    ginsum += sir[1];
                    binsum += sir[2];
                } else {
                    routsum += sir[0];
                    goutsum += sir[1];
                    boutsum += sir[2];
                }

                if (i < hm) {
                    yp += w;
                }
            }
            yi = x;
            stackpointer = radius;
            for (y = 0; y < h; y++) {
                // Preserve alpha channel: ( 0xff000000 & pix[yi] )
                pix[yi] = (0xff000000 & pix[yi]) | (dv[rsum] << 16) | (dv[gsum] << 8) | dv[bsum];

                rsum -= routsum;
                gsum -= goutsum;
                bsum -= boutsum;

                stackstart = stackpointer - radius + div;
                sir = stack[stackstart % div];

                routsum -= sir[0];
                goutsum -= sir[1];
                boutsum -= sir[2];

                if (x == 0) {
                    vmin[y] = Math.min(y + r1, hm) * w;
                }
                p = x + vmin[y];

                sir[0] = r[p];
                sir[1] = g[p];
                sir[2] = b[p];

                rinsum += sir[0];
                ginsum += sir[1];
                binsum += sir[2];

                rsum += rinsum;
                gsum += ginsum;
                bsum += binsum;

                stackpointer = (stackpointer + 1) % div;
                sir = stack[stackpointer];

                routsum += sir[0];
                goutsum += sir[1];
                boutsum += sir[2];

                rinsum -= sir[0];
                ginsum -= sir[1];
                binsum -= sir[2];

                yi += w;
            }
        }

        Log.e(TAG, w + " " + h + " " + pix.length);
        bitmap.setPixels(pix, 0, w, 0, 0, w, h);

        return (bitmap);
    }

    public static void compressImage(String originalImagePath, String compressedImagePath,
                                     int reqWidth, int reqHeight, int reqQuality) {
        Log.d(TAG, "Processing image + " + originalImagePath);
        OutputStream os_large;
        try {
            os_large = new FileOutputStream(compressedImagePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        }

        // Resize the image
        Bitmap compressed_ori;
        if (reqWidth == 1080) {
            compressed_ori = decodeSampledBitmapFromFile(originalImagePath, null);
        } else {
            compressed_ori = ImageResizer.decodeSampledBitmapFromFile(originalImagePath, reqWidth, reqHeight, null);
        }

        // Set the orientation parameter
        Bitmap compressed = ImageUtil.rotateBitmap(originalImagePath, compressed_ori);
        if (compressed != compressed_ori)
            compressed_ori.recycle();

        if (compressed != null) {
            compressed.compress(Bitmap.CompressFormat.JPEG, reqQuality, os_large);
            compressed.recycle();

            //Close Image stream
            try {
                os_large.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static Bitmap decodeSampledBitmapFromFile(String filename, ImageCache cache) {

        final BitmapFactory.Options options = new BitmapFactory.Options();

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);

        options.inSampleSize = calculateInSampleSize(options);

        if (options.outWidth > 1080) {
            return getResizedBitmap(BitmapFactory.decodeFile(filename), 1080);
        }

        addInBitmapOptions(options, cache);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filename, options);
    }

    public static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options) {
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (width > 1080) {
            inSampleSize = (int) Math.floor((double) width / 1080);
        }
        return inSampleSize;
    }

    private static void addInBitmapOptions(BitmapFactory.Options options, ImageCache cache) {
        options.inMutable = true;

        if (cache != null) {
            Bitmap inBitmap = cache.getBitmapFromReusableSet(options);

            if (inBitmap != null) {
                options.inBitmap = inBitmap;
            }
        }
    }

    public static Bitmap flip(Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        if (bitmap != null) {
            Matrix matrix = new Matrix();
            matrix.preScale(-1.0f, 1.0f);
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();
            Bitmap new_bitmap = Bitmap.createBitmap(bitmap, 0, 0, width, height, matrix, true);
            // careful, as new_bitmap is sometimes not a copy!
            if (new_bitmap != bitmap) {
                bitmap.recycle();
                bitmap = new_bitmap;
            }
        }
        return bitmap;
    }
}
