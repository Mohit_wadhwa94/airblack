/*
 * Copyright (C) 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mohit.airblack.images;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;

import java.io.FileDescriptor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ImageResizer extends ImageWorker {
    private static final String TAG = "ImageResizer";
    protected int mImageWidth;
    protected int mImageHeight;

    protected boolean mScaleCrop;
    protected int mRotationDegree = 0;

    /**
     * Initialize providing a single target image size (used for both width and height);
     *
     * @param context
     * @param imageWidth
     * @param imageHeight
     */
    public ImageResizer(Context context, int imageWidth, int imageHeight) {
        super(context);
        setImageSize(imageWidth, imageHeight);
    }

    /**
     * Initialize providing a single target image size (used for both width and height);
     *
     * @param context
     * @param imageSize
     */
    public ImageResizer(Context context, int imageSize) {
        super(context);
        setImageSize(imageSize);
    }

    /**
     * Set the target image width and height.
     *
     * @param width
     * @param height
     */
    public void setImageSize(int width, int height) {
        mImageWidth = width;
        mImageHeight = height;
    }

    /**
     * Set the target image size (width and height will be the same).
     *
     * @param size
     */
    public void setImageSize(int size) {
        setImageSize(size, size);
    }

    public void scaleCropImage(boolean ScaleCrop) {
        mScaleCrop = ScaleCrop;
    }

    public void rotateImage(int degree) {
        mRotationDegree = degree;
    }

    public static Bitmap rotateBitmap(Bitmap source, int angle) {
        if ( source == null ) {
            return null;
        }

        if ( angle == 0 ) {
            return source;
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }

    public static Bitmap scaleCropBitmap(Bitmap bmp, boolean scaleCrop) {
        if ( !scaleCrop )
            return bmp;

        if ( bmp == null ) {
            Log.e(TAG, "Error, null bitmap received in function scaleCropBitmap");
            return null;
        }

        int width = bmp.getWidth();
        int height = bmp.getHeight();

        Rect original = new Rect(0,0, width, height);
        Rect transformed = new Rect();

        int newSize = Math.min(width, height);
        Gravity.apply(Gravity.CENTER, newSize, newSize, original, transformed);
        return Bitmap.createBitmap(bmp, transformed.left, transformed.top, transformed.width(), transformed.height());
    }

    /**
     * The main processing method. This happens in a background task. In this case we are just
     * sampling down the bitmap and returning it from a resource.
     *
     * @param resId
     * @return
     */
    private Bitmap processBitmap(int resId) {
        return decodeSampledBitmapFromResource(mResources, resId, mImageWidth,
                mImageHeight, getImageCache());
    }

    /**
     * The main processing method. This happens in a background task. In this case we are just
     * sampling down the bitmap and returning it from a resource.
     *
     * @param filename
     * @return
     */
    private Bitmap processBitmap(String filename) {
        Bitmap bmp = decodeSampledBitmapFromFile(filename, mImageWidth,
                mImageHeight, getImageCache());
        bmp = rotateBitmap(filename, bmp);
        return scaleCropBitmap(rotateBitmap(bmp, mRotationDegree), mScaleCrop);
    }

    private static Bitmap rotateBitmap(String fileName, Bitmap bmp) {
        if ( bmp == null ) {
            return null;
        }

        Matrix mtx = new Matrix();
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(fileName);
        } catch ( IOException e ) {
            e.printStackTrace();
            return bmp;
        }

        Bitmap rotateBitmap = bmp;

        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        Log.d(TAG, "Orientation " + orientation);
        switch (orientation) {
            case ExifInterface.ORIENTATION_UNDEFINED: // Nexus 7 landscape...
                break;
            case ExifInterface.ORIENTATION_NORMAL: // landscape
                break;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                mtx.preRotate(180);
                rotateBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mtx, false);
                mtx = null;
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                break;
            case ExifInterface.ORIENTATION_ROTATE_90: // portrait
                mtx.preRotate(90);
                rotateBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mtx, false);
                mtx = null;
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                break;
            case ExifInterface.ORIENTATION_ROTATE_270: // might need to flip horizontally too...
                mtx.preRotate(270);
                rotateBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mtx, false);
                mtx = null;
                break;
            default:
                mtx.preRotate(90);
                rotateBitmap = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), mtx, false);
                mtx = null;
                break;
        }

        if ( rotateBitmap != bmp ) {
            bmp.recycle();
        }

        return rotateBitmap;
    }

    @Override
    protected Bitmap processBitmap(Object data) {
        if ( data instanceof String ) {
            return processBitmap(String.valueOf(data));
        } else if ( data instanceof Integer ) {
            return processBitmap((int) data);
        } else {
            throw new IllegalArgumentException("Data is not a path or a resource");
        }
    }

    /**
     * Decode and sample down a bitmap from resources to the requested width and height.
     *
     * @param res The resources object containing the image data
     * @param resId The resource id of the image data
     * @param reqWidth The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @param cache The ImageCache used to find candidate bitmaps for use with inBitmap
     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
     *         that are equal to or greater than the requested width and height
     */
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
            int reqWidth, int reqHeight, ImageCache cache) {

        // BEGIN_INCLUDE (read_bitmap_dimensions)
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // END_INCLUDE (read_bitmap_dimensions)

        // If we're running on Honeycomb or newer, try to use inBitmap
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB ) {
            addInBitmapOptions(options, cache);
        }

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /**
     * Decode and sample down a bitmap from a file to the requested width and height.
     *
     * @param filename The full path of the file to decode
     * @param reqWidth The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @param cache The ImageCache used to find candidate bitmaps for use with inBitmap
     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
     *         that are equal to or greater than the requested width and height
     */
    public static Bitmap decodeSampledBitmapFromFile(String filename,
            int reqWidth, int reqHeight, ImageCache cache) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();

        if ( reqWidth < 0 || reqHeight < 0 ) {
            options.inJustDecodeBounds = false;
            return BitmapFactory.decodeFile(filename, options);
        }

        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // If we're running on Honeycomb or newer, try to use inBitmap
        addInBitmapOptions(options, cache);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(filename, options);
    }

    /**
     * Decode and sample down a bitmap from a file input stream to the requested width and height.
     *
     * @param fileDescriptor The file descriptor to read from
     * @param reqWidth The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @param cache The ImageCache used to find candidate bitmaps for use with inBitmap
     * @return A bitmap sampled down from the original with the same aspect ratio and dimensions
     *         that are equal to or greater than the requested width and height
     */
    public static Bitmap decodeSampledBitmapFromDescriptor(
            FileDescriptor fileDescriptor, int reqWidth, int reqHeight, ImageCache cache) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;

        // If we're running on Honeycomb or newer, try to use inBitmap
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.HONEYCOMB ) {
            addInBitmapOptions(options, cache);
        }

        return BitmapFactory.decodeFileDescriptor(fileDescriptor, null, options);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static void addInBitmapOptions(BitmapFactory.Options options, ImageCache cache) {
        //BEGIN_INCLUDE(add_bitmap_options)
        // inBitmap only works with mutable bitmaps so force the decoder to
        // return mutable bitmaps.
        options.inMutable = true;

        if (cache != null) {
            // Try and find a bitmap to use for inBitmap
            Bitmap inBitmap = cache.getBitmapFromReusableSet(options);

            if (inBitmap != null) {
                options.inBitmap = inBitmap;
            }
        }
        //END_INCLUDE(add_bitmap_options)
    }

    /**
     * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link BitmapFactory}. This implementation calculates
     * the closest inSampleSize that is a power of 2 and will result in the final decoded bitmap
     * having a width and height equal to or larger than the requested width and height.
     *
     * @param options An options object with out* params already populated (run through a decode*
     *            method with inJustDecodeBounds==true
     * @param reqWidth The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    public static int calculateInSampleSize(BitmapFactory.Options options,
            int reqWidth, int reqHeight) {
        // BEGIN_INCLUDE (calculate_sample_size)
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            long totalPixels = width * height / ( inSampleSize * inSampleSize );

            // Anything more than 2x the requested pixels we'll sample down further
            final long totalReqPixelsCap = reqWidth * reqHeight * 2;
            while (totalPixels > totalReqPixelsCap) {
                inSampleSize++;
                totalPixels = width * height / ( inSampleSize * inSampleSize );
            }
        }
        return inSampleSize;
    }

    public static void compressImage(String originalImagePath, String compressedImagePath,
                                     int reqWidth, int reqHeight, int reqQuality) {
        Log.d(TAG, "Processing image + " + originalImagePath);
        OutputStream os_large;
        try {
            os_large = new FileOutputStream(compressedImagePath);
        } catch ( FileNotFoundException e ) {
            e.printStackTrace();
            return;
        }

        // Resize the image
        Bitmap compressed_ori = ImageResizer.decodeSampledBitmapFromFile(originalImagePath, reqWidth, reqHeight, null);

        // Set the orientation parameter
        Bitmap compressed = rotateBitmap(originalImagePath, compressed_ori);
        if ( compressed != compressed_ori ) {
            compressed_ori.recycle();
        }

        if ( compressed != null ) {
            compressed.compress(Bitmap.CompressFormat.JPEG, reqQuality, os_large);
            compressed.recycle();

            //Close Image stream
            try {
                os_large.close();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
    }
}
