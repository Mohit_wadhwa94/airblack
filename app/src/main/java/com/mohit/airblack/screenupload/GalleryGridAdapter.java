package com.mohit.airblack.screenupload;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.mohit.airblack.R;
import com.mohit.airblack.common.BitmapUtils;
import com.mohit.airblack.common.Utility;
import com.mohit.airblack.images.ImageCache;
import com.mohit.airblack.images.ImageResizer;

import java.io.File;
import java.util.ArrayList;

public class GalleryGridAdapter extends RecyclerView.Adapter {

    private AppCompatActivity activity;
    private ArrayList<Image> imageList = new ArrayList<>();
    private ImageResizer mImageLoader;

    public GalleryGridAdapter(AppCompatActivity activity) {
        this.activity = activity;

        int width = Utility.getActivityWidth(activity);
        initImageLoader(width / 3);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.gallery_image_grid_entity, viewGroup, false);
        return new ImageViewHolder(view, activity);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        if (viewHolder instanceof ImageViewHolder) {
            ((ImageViewHolder) viewHolder).setImageResizer(mImageLoader);
            ((ImageViewHolder) viewHolder).setImage(imageList.get(i).getPath(), imageList.get(i).getThumb(),
                    imageList.get(i).getOrientation());
        }
    }

    @Override
    public int getItemCount() {
        return imageList.size();
    }

    public void initImageLoader(int image_size) {
        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(activity, "thumbs");
        cacheParams.setMemCacheSizePercent(0.25f);
        cacheParams.diskCacheEnabled = false;

        mImageLoader = new ImageResizer(activity, image_size);
        mImageLoader.scaleCropImage(true);
        mImageLoader.addImageCache(activity.getSupportFragmentManager(), cacheParams);
        mImageLoader.setImageFadeIn(false);
    }

    private static class Image {
        String path;
        String thumb;
        int orientation;

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public int getOrientation() {
            return orientation;
        }

        public void setOrientation(int orientation) {
            this.orientation = orientation;
        }
    }


    public int addImage(String image, String thumb, int orientation) {
        Image image1 = new Image();
        image1.setPath(image);
        image1.setThumb(thumb);
        image1.setOrientation(orientation);
        imageList.add(image1);
        return imageList.size();
    }

    public static class ImageViewHolder extends RecyclerView.ViewHolder implements BitmapUtils.BitmapLoadCallback {
        private static final String TAG = "ImageViewHolder";
        private ImageView imageView;
        private String mPath;
        private Activity activity;

        private ImageResizer imageResizer;

        public ImageViewHolder(View itemView, Activity activity) {
            super(itemView);
            this.activity = activity;
            imageView = itemView.findViewById(R.id.gallery_image_view);
        }

        public void setImage(String path, String thumb, int orientation) {
            mPath = path;

            imageView.setImageBitmap(null);
            imageView.invalidate();

            imageResizer.loadImage(mPath, imageView);

            itemView.setOnClickListener(v -> {
                Uri imageUri = Uri.fromFile(new File(mPath));
                Intent intent = new Intent(activity, ImageUploadActivity.class);
                intent.putExtra("image", imageUri);
                activity.startActivity(intent);
                activity.finish();
            });
        }

        @Override
        public void onBitmapLoaded(Bitmap bitmap, File file) {
            Log.d(TAG, "Loaded image file - " + file.getAbsolutePath());
            if (file.getAbsolutePath().equals(new File(mPath).getAbsolutePath())) {
                if (bitmap != null && !bitmap.isRecycled()) {
                    bitmap.recycle();
                    bitmap = null;
                }

                imageView.setImageBitmap(bitmap);
            } else {
                Log.e(TAG, "Image has been recycled");
            }
        }

        public void setImageResizer(ImageResizer imageResizer) {
            this.imageResizer = imageResizer;
        }
    }
}