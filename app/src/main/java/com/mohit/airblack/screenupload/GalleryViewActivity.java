package com.mohit.airblack.screenupload;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mohit.airblack.R;
import com.mohit.airblack.common.Utility;

public class GalleryViewActivity extends AppCompatActivity {


    public static final String TAG = GalleryViewActivity.class.getName();

    private RecyclerView mGridRecyclerView;
    private GalleryGridAdapter mImageAdapter;
    private ProgressBar progressBar;
    private LinearLayout noMediaLL;

    private AsyncImageLoader mImageLoader;
    private boolean isDestroyed;
    private static final int STORAGE_READ_DATA = 1000;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.gallery_view_activity);

        isDestroyed = false;

        init();
    }

    private void init() {
        mGridRecyclerView = findViewById(R.id.gallery_images_recycler_view);
        progressBar = findViewById(R.id.progressBar);
        noMediaLL = findViewById(R.id.noMediaLL);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 4,
                RecyclerView.VERTICAL, false);
        mGridRecyclerView.setLayoutManager(layoutManager);
        mImageAdapter = new GalleryGridAdapter(this);
        mGridRecyclerView.setAdapter(mImageAdapter);

        if (Utility.isPermissionGranted(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            loadData();

        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_READ_DATA);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == STORAGE_READ_DATA && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            hideEmptyLayout();
            loadData();
        } else {
            showEmptyImagesLayout();
        }

    }

    public void loadData() {
        mImageLoader = new AsyncImageLoader(this, 0, this);
        mImageLoader.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isDestroyed = true;
    }

    public void onImageLoadComplete(final String image, final String thumb, final int orientation) {
        if (isDestroyed) {
            return;
        }

        new Handler(Looper.getMainLooper()).post(() -> {
            mImageAdapter.addImage(image, thumb, orientation);
            mImageAdapter.notifyDataSetChanged();
        });
    }

    private class AsyncImageLoader extends Thread {
        private GalleryViewActivity callback;
        long bucketId;
        private Context context;
        boolean stop;

        public AsyncImageLoader(GalleryViewActivity callback, long bucketId, Context context) {
            this.callback = callback;
            this.bucketId = bucketId;
            this.context = context;
            stop = false;
        }

        public void stopThread() {
            stop = true;
        }

        private Cursor getCursor(int bucketId, Context ctxt) {
            final String[] PROJECTION_BUCKET = {
                    MediaStore.Images.ImageColumns.BUCKET_ID,
                    MediaStore.Images.ImageColumns.BUCKET_DISPLAY_NAME,
                    MediaStore.Images.Media._ID,
                    MediaStore.Images.Media.DATA,
                    MediaStore.Images.Thumbnails.DATA,
                    MediaStore.Images.ImageColumns.ORIENTATION,
                    MediaStore.Images.Media.DATE_ADDED,
            };

            String WHERE_CLAUSE = MediaStore.Images.ImageColumns.BUCKET_ID + " = " + bucketId;
            if (bucketId == 0) WHERE_CLAUSE = null;
            String BUCKET_ORDER_BY = MediaStore.Images.ImageColumns.DATE_TAKEN + " DESC ";
            Uri uriMedia = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
            try {
                return ctxt.getContentResolver().query(uriMedia, PROJECTION_BUCKET, WHERE_CLAUSE, null, BUCKET_ORDER_BY);
            } catch (Exception e) {
                return null;
            }
        }

        private void getLocalImages() {
            Cursor imageCursor = getCursor((int) bucketId, context);
            try {

                if (imageCursor == null || imageCursor.getCount() == 0) {
                    showEmptyImagesLayout();
                    return;
                }

                while (imageCursor.moveToNext()) {
                    if (stop) {
                        Log.e(TAG, "Stopping thread");
                        break;
                    }

                    String path = imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Images.Media.DATA));
                    String thumb = imageCursor.getString(imageCursor.getColumnIndex(MediaStore.Images.Thumbnails.DATA));
                    int orientation = imageCursor.getInt(imageCursor.getColumnIndex(MediaStore.Images.ImageColumns.ORIENTATION));

                    if (path != null && !path.contains("/WPSystem/")) {
                        callback.onImageLoadComplete(path, thumb, orientation);
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (imageCursor != null && !imageCursor.isClosed()) {
                    imageCursor.close();
                }
            }
        }

        @Override
        public void run() {
            stop = false;
            if (Utility.isPermissionGranted(GalleryViewActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                try {
                    getLocalImages();
                } catch (SecurityException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void showEmptyImagesLayout() {
        if (mImageAdapter.getItemCount() == 0 && !isDestroyed) {
            mGridRecyclerView.setVisibility(View.GONE);
            noMediaLL.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        } else {
            mGridRecyclerView.setVisibility(View.VISIBLE);
            noMediaLL.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
        }
    }

    private void hideEmptyLayout() {
        mGridRecyclerView.setVisibility(View.VISIBLE);
        noMediaLL.setVisibility(View.GONE);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onStop() {
        super.onStop();

        if (mImageLoader != null) {
            mImageLoader.stopThread();
        }
    }
}