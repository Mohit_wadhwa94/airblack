package com.mohit.airblack.screenupload;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.mohit.airblack.screenhome.HomeActivity;
import com.mohit.airblack.R;
import com.mohit.airblack.common.FragmentUtils;
import com.mohit.airblack.common.Utility;
import com.mohit.airblack.data.album.ImageUploadData;

public class ImageUploadActivity extends AppCompatActivity {

    private Uri imageUri;
    FirebaseStorage storage;
    StorageReference storageReference;
    DatabaseReference databaseReference;
    String storagePath = "images/";
    public static final String databasePath = "images_database/";

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.upload_image_activity);

        init();
        handleIntent();
        setData();
    }

    private void init() {
        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();
        databaseReference = FirebaseDatabase.getInstance().getReference(databasePath);
    }

    private void handleIntent() {
        Intent intent = getIntent();

        if (intent == null) {
            return;
        }

        imageUri = intent.getParcelableExtra("image");
    }

    private void setData() {
        if (imageUri == null) {
            Utility.showToast(this, "Image is not valid, please try again");
            finish();
            return;
        }

        openImageEditFragment();
    }

    private void openImageEditFragment() {

        ImageEditFragment fragment = (ImageEditFragment) getSupportFragmentManager()
                .findFragmentByTag(ImageEditFragment.TAG);
        if (fragment != null) {
            FragmentUtils.popBackStackAllowingStateLosses(getSupportFragmentManager());
        }

        fragment = new ImageEditFragment();
        Bundle arg = new Bundle();

        arg.putString("image", imageUri.getPath());
        fragment.setArguments(arg);

        getSupportFragmentManager().beginTransaction()
                .addToBackStack(null)
                .add(android.R.id.content, fragment, ImageEditFragment.TAG)
                .commitAllowingStateLoss();
    }

    public void uploadImage(Uri filePath) {
        if (filePath != null) {

            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();
            progressDialog.setTitle("Image is Uploading...");
            progressDialog.show();

            StorageReference storageReference2nd = storageReference.child(storagePath + System.currentTimeMillis() + ".jpg");

            storageReference2nd.putFile(filePath)
                    .addOnSuccessListener(taskSnapshot -> {
                        storageReference2nd.getDownloadUrl().addOnSuccessListener(uri -> {

                            String TempImageName = filePath.getPath();
                            progressDialog.dismiss();
                            Utility.showToast(ImageUploadActivity.this, "Image Uploaded");
                            ImageUploadData imageUploadData = new ImageUploadData(TempImageName, uri.toString());
                            String ImageUploadId = databaseReference.push().getKey();
                            databaseReference.child(ImageUploadId).setValue(imageUploadData);
                            Intent intent = new Intent(ImageUploadActivity.this, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        });

                    })
                    .addOnFailureListener(exception -> {
                        progressDialog.dismiss();
                        Utility.showToast(ImageUploadActivity.this, "Image upload failed");
                    })

                    .addOnProgressListener(taskSnapshot -> {

                        progressDialog.setTitle("Image is Uploading...");

                    });
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
