package com.mohit.airblack.screenupload;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.mohit.airblack.R;
import com.mohit.airblack.common.Utility;
import com.mohit.airblack.images.ImageCache;
import com.mohit.airblack.images.ImageResizer;
import com.naver.android.helloyako.imagecrop.view.ImageCropView;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class ImageEditFragment extends Fragment {

    public static final String TAG = ImageEditFragment.class.getName();

    private String mFileName;

    private FrameLayout mSurfaceFrame;
    private ImageCropView mFilterCropView;
    private ImageView mRotateButton;
    private ImageView mFlipButton;
    private View mDoneButton;
    private boolean isFlipped;

    private int rotation;
    private int rotationOld;
    private Bitmap mBitmapFull;
    private BitmapDrawable mBitmapDrawableFull;
    private int mSurfaceWith, mSurfaceHeight;
    private static final int STORAGE_WRITE_DATA = 1001;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mFileName = getArguments().getString("image");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View root = inflater.inflate(R.layout.image_filter_layout, container, false);
        root.setOnTouchListener((v, event) -> true);

        mSurfaceFrame = root.findViewById(R.id.filter_surface_frame);
        mFilterCropView = root.findViewById(R.id.filter_crop_view);
        mRotateButton = root.findViewById(R.id.rotate_button);
        mFlipButton = root.findViewById(R.id.flip_button);
        mDoneButton = root.findViewById(R.id.filter_done);

        mFilterCropView.setBackgroundColor(0x00000000);
        mFilterCropView.setScaleEnabled(false);
        mFilterCropView.setDoubleTapEnabled(false);
        mFilterCropView.setAspectRatio(1, 1);

        root.findViewById(R.id.filter_back).setOnClickListener(backClick);
        root.findViewById(R.id.filter_done).setEnabled(false);

        int screenWidth = Utility.getActivityWidth(getActivity());

        ImageCache.ImageCacheParams cacheParams = new ImageCache.ImageCacheParams(getActivity(), "thumbs");
        cacheParams.setMemCacheSizePercent(0.25f);

        ImageResizer fullImgResizer = new ImageResizer(getContext(), screenWidth);
        fullImgResizer.addImageCache(getFragmentManager(), cacheParams);
        fullImgResizer.setLoadCallback(bitmapLoadCallback);
        fullImgResizer.loadImage(mFileName, null);

        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    root.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }

                mSurfaceWith = mSurfaceFrame.getWidth();
                mSurfaceHeight = mSurfaceFrame.getHeight();
                updateSurfaceView();
            }
        });

        mRotateButton.setOnClickListener(view -> {
            fullImgResizer.setLoadCallback(bitmapLoadCallback);

            rotation = (rotation + 90) % 360;
            bitmapLoadCallback.onBitmapLoaded(mBitmapDrawableFull);
        });

        mFlipButton.setOnClickListener(view -> {
            fullImgResizer.setLoadCallback(bitmapFlipLoadCallback);
            bitmapFlipLoadCallback.onBitmapLoaded(mBitmapDrawableFull);
        });

        return root;
    }

    private ProgressDialog progressDialog;
    private View.OnClickListener doneClick = v -> uploadImage();

    private void uploadImage() {
        if (getActivity() == null || !isAdded()) {
            return;
        }

        if (!Utility.isPermissionGranted(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, STORAGE_WRITE_DATA);
            return;
        }

        String dialogMessage = "Processing image ...";
        progressDialog = new

                ProgressDialog(getActivity());
        progressDialog.setMessage(dialogMessage);

        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(false);
        progressDialog.show();


        Bitmap bitmap = mFilterCropView.getViewBitmap();

        Utility.dismissDialog(progressDialog);

        Uri uri = getImageUri(getContext(), bitmap);

        ((ImageUploadActivity) getActivity()).uploadImage(uri);
        new Handler().postDelayed(() -> {
            getActivity().getSupportFragmentManager().beginTransaction()
                    .remove(ImageEditFragment.this).commit();
        }, 400);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == STORAGE_WRITE_DATA && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            uploadImage();
        } else {
            Utility.showToast(getActivity(), "Please grant permissions to proceed");
        }
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private View.OnClickListener backClick = v -> {
        if (getActivity() == null) {
            return;
        }

        ((ImageUploadActivity) getActivity()).onBackPressed();
    };

    private ImageResizer.LoadCallback bitmapLoadCallback = new ImageResizer.LoadCallback() {
        @Override
        public void onBitmapLoaded(BitmapDrawable bitmapDrawable) {
            if (getActivity() == null || !isAdded()) {
                Log.e(TAG, "Fragment is no more");
                return;
            }

            if (bitmapDrawable == null || bitmapDrawable.getBitmap() == null) {
                Log.e(TAG, "Bitmap is null");
                return;
            }

            mBitmapDrawableFull = bitmapDrawable;
            mBitmapFull = bitmapDrawable.getBitmap();

            if (rotation - rotationOld != 0) {
                Matrix matrix = new Matrix();
                matrix.postRotate(rotation - rotationOld);

                Bitmap tmpBitmap = Bitmap.createBitmap(mBitmapFull, 0, 0, mBitmapFull.getWidth(),
                        mBitmapFull.getHeight(), matrix, true);
                if (tmpBitmap != mBitmapFull) {
                    mBitmapFull.recycle();
                }

                mBitmapFull = tmpBitmap;
                mBitmapDrawableFull = new BitmapDrawable(mBitmapFull);
                rotationOld = rotation;
            }

            updateSurfaceView();
        }
    };


    private ImageResizer.LoadCallback bitmapFlipLoadCallback = new ImageResizer.LoadCallback() {
        @Override
        public void onBitmapLoaded(BitmapDrawable bitmapDrawable) {
            if (getActivity() == null || !isAdded()) {
                Log.e(TAG, "Fragment is no more");
                return;
            }

            if (bitmapDrawable == null || bitmapDrawable.getBitmap() == null) {
                Log.e(TAG, "Bitmap is null");
                return;
            }

            mBitmapDrawableFull = bitmapDrawable;
            mBitmapFull = bitmapDrawable.getBitmap();

            if (mBitmapFull != null) {
                Matrix matrix = new Matrix();
                matrix.preScale(-1.0f, 1.0f);
                int width = mBitmapFull.getWidth();
                int height = mBitmapFull.getHeight();
                Bitmap tmpBitmap = Bitmap.createBitmap(mBitmapFull, 0, 0, width, height, matrix, true);
                // careful, as new_bitmap is sometimes not a copy!
                if (tmpBitmap != mBitmapFull) {
                    mBitmapFull.recycle();
                }
                mBitmapFull = tmpBitmap;

                mBitmapDrawableFull = new BitmapDrawable(mBitmapFull);
            }

            updateSurfaceView();

            if (isFlipped == false) {
                isFlipped = true;
            } else {
                isFlipped = false;
            }
        }
    };

    private void updateSurfaceView() {
        if (mBitmapFull == null || mSurfaceWith == 0 || mSurfaceHeight == 0) {
            Log.e(TAG, "Variables not yet set");
            return;
        }

        Bitmap bitmap = mBitmapFull;

        mDoneButton.setEnabled(true);
        mDoneButton.setOnClickListener(doneClick);

        if (bitmap.getWidth() >= bitmap.getHeight()) {
            mFilterCropView.setAspectRatio(bitmap.getWidth(), bitmap.getHeight());
        } else {
            mFilterCropView.setAspectRatio(1, 1);
        }

        mFilterCropView.setImageBitmap(bitmap);
    }
}
