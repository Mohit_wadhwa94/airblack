package com.mohit.airblack.init;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.StrictMode;

import androidx.multidex.MultiDex;

import com.mohit.airblack.common.Utility;

import java.util.UUID;

public class AppController extends ApplicationManager {


    public static final String TAG = "AppController";

    private static String sSessionId;

    @SuppressLint("StaticFieldLeak")
    private static AppController instance;

    public static AppController getInstance() {
        if (instance == null)
            throw new IllegalStateException();
        return instance;
    }

    public AppController() {
        instance = this;
    }

    public static String getSessionId() {
        if ( sSessionId == null ) {
            sSessionId = UUID.randomUUID().toString();
        }
        return sSessionId;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        initializeUtils();
    }

    private void initializeUtils() {
        Utility.initUtility(getApplicationContext());
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}