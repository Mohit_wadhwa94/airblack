package com.mohit.airblack.screenhome;

import android.content.Intent;
import android.os.Bundle;
import android.widget.FrameLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.mohit.airblack.R;
import com.mohit.airblack.screenupload.GalleryViewActivity;
import com.mohit.airblack.screenview.UploadedImagesActivity;

public class HomeActivity extends AppCompatActivity {

    private static final String TAG = HomeActivity.class.getName();
    FrameLayout uploadImages;
    FrameLayout viewImages;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initialize();
    }

    private void initialize() {
        uploadImages = findViewById(R.id.upload_button);
        viewImages = findViewById(R.id.view_button);

        uploadImages.setOnClickListener(view -> {
            Intent intent = new Intent(this, GalleryViewActivity.class);
            startActivity(intent);
        });

        viewImages.setOnClickListener(view -> {
            Intent intent = new Intent(this, UploadedImagesActivity.class);
            startActivity(intent);
        });
    }
}
