package com.mohit.airblack.data;

public interface ModelCallback<T> {

	void success(T response);
	
	void error(Throwable throwable);
	
}
