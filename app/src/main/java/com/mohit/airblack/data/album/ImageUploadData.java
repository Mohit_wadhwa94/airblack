package com.mohit.airblack.data.album;

import android.os.Parcel;
import android.os.Parcelable;

public class ImageUploadData implements Parcelable {


    public String imageName;

    public String imageURL;

    public ImageUploadData() {

    }

    public ImageUploadData(String name, String url) {

        this.imageName = name;
        this.imageURL = url;
    }

    public String getImageName() {
        return imageName;
    }

    public String getImageURL() {
        return imageURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.imageName);
        dest.writeString(this.imageURL);
    }

    protected ImageUploadData(Parcel in) {
        this.imageName = in.readString();
        this.imageURL = in.readString();
    }

    public static final Parcelable.Creator<ImageUploadData> CREATOR = new Parcelable.Creator<ImageUploadData>() {
        @Override
        public ImageUploadData createFromParcel(Parcel source) {
            return new ImageUploadData(source);
        }

        @Override
        public ImageUploadData[] newArray(int size) {
            return new ImageUploadData[size];
        }
    };
}
